<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProducerRepository")
 *
 * @ApiResource(
 *     attributes={"access_control"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "post"={"access_control"="is_granted('ROLE_ADMIN')", "access_control_message"="Only admins can add producers."}
 *     },
 * )
 */
class Producer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $administrative_name;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $lng;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $lat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="producers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdministrativeName(): ?string
    {
        return $this->administrative_name;
    }

    public function setAdministrativeName(?string $administrative_name): self
    {
        $this->administrative_name = $administrative_name;

        return $this;
    }

    public function getLng(): ?int
    {
        return $this->lng;
    }

    public function setLng(int $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getLat(): ?int
    {
        return $this->lat;
    }

    public function setLat(?int $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return Producer
     */
    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }




}
