<?php

namespace App\Controller;

use App\Entity\Producer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class ProducerController extends AbstractController
{
    /**
     * @Route("/producer", name="producer")
     */
    public function index()
    {
        return $this->render('producer/index.html.twig', [
            'controller_name' => 'ProducerController',
        ]);
    }





    /**
     * @Route("/add", name="producer_add", methods={"POST"})
     */
    public function add(Request $request)
    {
     $serializer = $this->get('serializer');

     $producer = $serializer->deserialize($request->getContent(), Producer::class, 'json');

     $em = $this->getDoctrine()->getManager();
     $em->persist($producer);
     $em->flush();

     return $this->json($producer);
    }
}
